#![feature(test)]

extern crate itertools;
extern crate test;

use itertools::Itertools;
use itertools::FoldWhile::{Continue, Done};
use test::Bencher;

const TEST_ARRAY: [u8; 10000] = [0; 10000];
const LEADING_ZEROS: [usize; 256] =
    [8, 7, 6, 6, 5, 5, 5, 5, 4, 4, 4, 4, 4, 4, 4, 4, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
     3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
     2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
     1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
     1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0];

fn leading_zeros_array(input: &[u8]) -> usize {
    input.iter().fold_while(0usize, |accumulated, x| {
        let leading_zeros = LEADING_ZEROS[*x as usize];
        if leading_zeros == 8 {
            Continue(accumulated + 8)
        } else {
            Done(accumulated + leading_zeros)
        }
    })
}

fn leading_zeros_match(input: &[u8]) -> usize {
    input.iter().fold_while(0usize, |accumulated, x| {
        match *x {
            0 => Continue(accumulated + 8),
            1 => Done(accumulated + 7),
            2 | 3 => Done(accumulated + 6),
            4...7 => Done(accumulated + 5),
            8...15 => Done(accumulated + 4),
            16...31 => Done(accumulated + 3),
            32...63 => Done(accumulated + 2),
            64...127 => Done(accumulated + 1),
            _ => Done(accumulated),
        }
    })
}

fn leading_zeros_native(input: &[u8]) -> usize {
    input.iter().fold_while(0usize, |accumulated, x| {
        let leading_zeros = x.leading_zeros() as usize;
        if leading_zeros == 8 {
            Continue(accumulated + 8)
        } else {
            Done(accumulated + leading_zeros)
        }
    })
}

fn leading_zeros_modulo(input: &[u8]) -> usize {
    input.iter().fold_while(0usize, |accumulated, x| {
        let leading_zeros = 8 - x % 8;
        if leading_zeros == 8 {
            Continue(accumulated + 8)
        } else {
            Done(accumulated + leading_zeros as usize)
        }
    })
}

#[bench]
fn using_array(bencher: &mut Bencher) {
    bencher.iter(|| leading_zeros_array(&TEST_ARRAY));
}

#[bench]
fn using_match(bencher: &mut Bencher) {
    bencher.iter(|| leading_zeros_match(&TEST_ARRAY));
}

#[bench]
fn using_native(bencher: &mut Bencher) {
    bencher.iter(|| leading_zeros_native(&TEST_ARRAY));
}

#[bench]
fn using_modulo(bencher: &mut Bencher) {
    bencher.iter(|| leading_zeros_modulo(&TEST_ARRAY));
}
