Comparison of approaches to calculate the number of leading zeros when treating a `[u8]` as
representing a single, large binary number .

The alternative approaches for each `u8` are:

1. using an array containing the result for each possible value of `u8`
1. using a `match` statement
1. using a modulo calculation
1. using the native `leading_zeros()` function

The benchmarks tested against an array of 10,000 `0u8`s - probably an unlikely scenario, but
required to allow the tests to register a non-zero timing.

    test using_array  ... bench:       1,603 ns/iter (+/- 20)
    test using_match  ... bench:       1,440 ns/iter (+/- 15)
    test using_modulo ... bench:       1,601 ns/iter (+/- 10)
    test using_native ... bench:       1,608 ns/iter (+/- 93)
